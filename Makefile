###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05a - readelf - EE 491F - Spr 2022
###
### @file    Makefile
### @version 1.0 - Copied from memory scanner
###
### Build readelf application.
###
### @author  Patrick McCrindle <pmccrind@hawaii.edu>
### @date    20_Mar_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = readelf

all: $(TARGET)

readelf: readelf.c
	$(CC) -B  $(CFLAGS) readelf_functions.c  -o $(TARGET) readelf.c

clean:
	rm -f $(TARGET)

test:
	./readelf 

new: clean
	$(MAKE)
