/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491 - Spr 2022
///
/// @file    readelf.c
/// @version 0.1 - copied from wc.c
///
/// readelf - print out the readelf header information
//
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    20_Mar_2022
///
/// @see    https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include "readelf.h"

// Need Argc and argv
// @argc - argument count passed to the program, expected 1 or more
// @argv - argument list passed to the program, expected 
// ["./readelf", "somefile.txt", "-h"]
int main( int argc, char *argv[] ) {
   
   //Commandline Parsing will be completed here

   //three single flags: -c, -l, OR -w
   //four double flags: --bytes, --lines, --words, OR 
   //--version

   // No File Provided read from stdin
   // No flags either provide standard output
   // Return error
   if (argc == 1) {
      
      //File did not open successfully
      fprintf(stderr, "%s: Warning nothing to do.\n", FILENAME);
      
      // @TODO print out the avialable flags and description of them

      //Exit with failure
      exit(EXIT_FAILURE);


   } else {
      // Two or more arguments recieved, need to find all valid flag commandline
      // pass to parse commandline argument parser.
   
      Arguments returnedArgs;

      returnedArgs = parseCommandLineArguments( argc, argv );      

      //Check to make sure the h flag is true
      // @TODO add in checks for all future flags
      if ( !returnedArgs.hFlag ){
         fprintf( stderr, "%s: the -h flag is required.\n", FILENAME );
         //exit with failure
         exit(EXIT_FAILURE);

      }
      
      // header flag set need to make sure the file supplied is an elf file
      

      // open each of the files returned by the parsing function
      if ( returnedArgs.filed == 0  ){
         
         //No files returned error and exit
         fprintf( stderr, "%s: Warning nothing to do. \n", FILENAME );
         //exit with failure
         exit( EXIT_FAILURE );
      }
      
      // String used to check if the program is a readelf program or
      // not.
      char elfString[] = {0x7f, 0x45, 0x4c, 0x46,  0x00};
         
      // Stores the Constant elf in the String
      char elfConstant[8];
       
      // Converts the second string and stores in the first
      stringToHex( elfConstant, elfString, sizeof(elfString) - 1 );   
      
      // For comparing to the number of files returned
      int filing = 0;

      //File pointer for the incoming files
      FILE* fileptr;
     
      // Stores the files name
      char filename[512];

      // Open all of the files found with getopt().
      while ( filing < returnedArgs.filed  ) {
         
         // Copy from the returned array in the struct to 
         // file for opening.
         strcpy( filename, returnedArgs.files[filing] );
         
         fileptr = fopen ( filename, "r" );

         //file couldn't be opened
         if ( fileptr == NULL ){
         
            fprintf( stderr, "%s: File [%s] could not be opened for processing.\n", FILENAME, filename);
            
            //increment loop
            filing++;

            // continue to try and open other files if any
            continue;
         } 


         // header flag set need to make sure the file supplied is an elf file
         // Need to check the first 4 bytes to see if they are 7F 45 4C 46
         
         // grab the first 4 chars making sure it matches
         char c = ' ';

         char stringCheck[5];

         int i = 0;
         //copy in the first 4 chars into string
         for ( i = 0; i < 4; i++ ) { 
            c = fgetc ( fileptr );

            stringCheck[i] = c;
            
         } // end for loop

         
         // convert the given string to hex
         
         // String to store the converted string in hex
         char hexFromFile [5];
         
         // Converts the second string and stores in the first
         stringToHex ( hexFromFile, stringCheck, sizeof(stringCheck) - 1 );
         


         // stringCheck now holds the first 4 bytes of the progam, compares to make sure it is
         // the correct 4 values.
         if ( strcmp( hexFromFile, elfConstant ) != 0  ){
            // strings not equal exit this while loop run and move on
            fprintf( stderr, "%s: [%s] is not a readelf file. \n", FILENAME, filename );
            
            //increment loop
            filing++;
            //continue to next file if any
            continue;
         }
         
         // File is a readelf file. Parse the rest of file.
         
         printf( "ELF Header: \n" );
         printf( "Magic: " );
          
         //print out the first 13 bytes
         
         //Put file pointer at start of the file.
         fseek (fileptr, 0, SEEK_SET );
         
         c = ' ';
         char magic[14];
         for (i = 0; i < 13; i++){
            
            c = fgetc( fileptr );
            printf( "%02X ", c  );
            // save into the buffer
            magic[i] = c;
         }// end for loop to print MAGIC header values
         
         magic[i] = '\0';
         printf("\n");
         
         //determine the class
         if ( magic[4] == 1 ){
            // 32 bit machine
            printf( "\tClass: \t\t\t ELF32\n" );
         
         } else if (magic[4] == 2) {
            // 64 bit machine

            printf( "\tClass: \t\t\t ELF64\n" );

         } else {
            // Error exit program

            fprintf( stderr, "%s: wrong class number, only 1 or 2 is allowed.\n", FILENAME );
            // Exit
            exit( EXIT_FAILURE );

         }

         //Determine little or big endian
         bool bigEndian = false; 
         if ( magic[5] == 1 ){
            // Little Endian

            printf( "\tData: \t\t\t two's compliment, little endian\n" );

         } else if ( magic[5] == 2 ){
            // Big Endian

            printf( "\tData: \t\t\t two's compliment, big endian\n" );
            bigEndian = true;

         } else {

            fprintf( stderr, "%s: wrong data number, only 1 or 2 is allowed.\n", FILENAME );
            // Exit
            exit( EXIT_FAILURE );

         }// end little or big endian


         // Check version
         if (magic[6] != 1){
            // No other versions allowed.
            fprintf( stderr, "%s: only version 1 of readelf is allowed.\n", FILENAME );
            //exit with error
            exit( EXIT_FAILURE );

         }// End Version check
          
         printf( "\tVersion: \t\t 1 (current)\n" );


         //close the file
         int fileClosed = fclose( fileptr );

         if ( fileClosed != 0 ){
            //error with closing
            fprintf( stderr, "%s: File [%s] could not be closed properly. \n", FILENAME, filename );
            // exit with failure
            exit( EXIT_FAILURE );
         }

         //File processed successfully increment loop and head back to the top
         filing++;
      }//end loop over files

      //Exit with success
      exit(EXIT_SUCCESS);

   }//end argc >= 2 case
      
   //Should not reach this section,
   //exit with failure if you do.
   exit(EXIT_FAILURE);

}//End Main Function
 

