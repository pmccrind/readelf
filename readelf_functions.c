/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491F - Spr 2022
///
/// @file    readelf_functions.c
/// @version 0.5 - copied from memory scanner
///
/// readelf_functions - Holds required functions for readelf
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    20_Mar_2022
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <stdlib.h>
#include <stdbool.h>
#include "readelf.h"

// Function to do commandline parsing.
// Using hint provided in the lab manual to use
// getopt.

// @TODO create a struct and return all requested
// flags.
// Current returns true if h flag is set and false
// otherwise.
Arguments parseCommandLineArguments( int argc, char* argv[] ){

   int getOpt = 0;
   

   // Structure to store the information to be returned
   // Struct is 0'd out
   Arguments arg = { false, { " " }, 0 };

   // getopt returns -1 when there are no more commands
   // to parse
   // The "h" will need to be changed for all the flags
   // that are to be checked and set by getopt.
   // For flags that need a option place a ':' after that
   // flag.
   // @TODO add in other flags, make all flags optionals
   // check for file(s) at the end.
   while ( ( getOpt = getopt( argc, argv, ":h") ) != -1  ) {

      // switch not needed for 1 check but built for future
      // needed checks
      switch( getOpt  ){
         
         // check for h flag
         case 'h':
            arg.hFlag = true;
            break;
         
         case '?':
            // not a valid argument
            fprintf( stderr, "Unknown argument [%c]\n", optopt );
            // exit
            exit(EXIT_FAILURE);

         case ':':
            // missing argument for h flag 
            fprintf ( stderr, "Missing an argument for %c\n", optopt );
            // exit
            exit(EXIT_FAILURE);
      
      }//end switch

   }// end while loop

   
   // Used to hold the number of potential files to open
   int i = 0;
   // places the files from getopt() into the array in the
   // args struct
   while ( optind < argc  ){
      
      
      strcpy( arg.files[i], argv[optind++] );
      //Increase i to store in the next position
      i++;
      //increase the count for the number of files
      arg.filed = i;
   }//end while
   
   return arg;
}// end parseCommandLineArguments Function
 
 



// Convert the given String to its hex values
void stringToHex ( char* hexString, char* incomingString, int length ){
   
   // For loop
   int i = 0;
   // Hex String
   int j = 0;

   for ( i = 0; i < length; i++ ){
            
      sprintf( hexString + j, "%02x", incomingString[i] & 0xff );
      j += 2;
   
   }
   hexString[j] = '\0';   
   // String converted to hex

}// End String to Hex Function


 
