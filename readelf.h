/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491 - Spr 2022
///
/// @file    readelf.h
/// @version 0.1 - copied from wc.h
///
/// readelf - display the information from headers of programs
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    20_Mar_2022
///
///////////////////////////////////////////////////////////////////////////////

#define VERSION "\n readelf 1.0 \n Copyright Patrick McCrindle\n"
#define FILENAME "readelf"

extern char ABI[15][30] = {

   {"System V"},
   {"Test"},

};


// Struct to pass the arry of files and the flags
// returned from parseCommandLineArguments
typedef struct {
   
   //start of flag list
   bool hFlag;

   // Array for list of files found by getopt()
   // Create an array of strings
   // @TODO notify users of the 10 file limit.
   char files[10][512];

   //number of files in files
   int filed;

} Arguments;


// Function header for parsing commandline arguments
// argc, number of arguments from main
// argv[] array of strings of all the commandline
// arguments
Arguments parseCommandLineArguments( int argc, char* argv[] );



// Function to convert the given string to hex
// needs a string and modifys the given string.
void stringToHex ( char* hexString, char* incomingString, int length );

